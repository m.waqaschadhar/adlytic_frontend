import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { BrandModalComponent } from 'src/app/modal/brand-modal/brand-modal.component';
import { UploadModalComponent } from 'src/app/modal/upload-modal/upload-modal.component';
import { RequestService } from 'src/app/request.service';

@Component({
  selector: 'app-brand',
  templateUrl: './brand.component.html',
  styleUrls: ['./brand.component.scss']
})
export class BrandComponent implements OnInit {
  baseUrl:string;
  brands:any[];
  constructor(public dialog: MatDialog, private requestService:RequestService) {}

  ngOnInit() {
    this.baseUrl = this.requestService.getBaseUrl() + 'uploads/';
    this.getBrands();
  }


  openDialog() {
    const dialogRef = this.dialog.open(BrandModalComponent, {
      height: 'auto',
      width: '400px',
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(result => {
      if(result==undefined){

      }else if(result==null){

      }else{
        this.addBrand(result);
      }
    });
  }

  addBrand(requestData){
    this.requestService.createBrand(requestData).subscribe(res=>this.addSuccess(res), err=>this.addError(err));
  }

  addSuccess(response:any){
    console.log(response);
  }

  addError(error:any){
    console.log(error);
  }

  getBrands(){
    this.requestService.getBrands().subscribe(res=>this.getSuccess(res), err=>this.getError(err));
  }

  getSuccess(response:any){
    console.log(response);
    this.brands = response;
  }

  getError(error:any){
    console.log(error);
  }

}
