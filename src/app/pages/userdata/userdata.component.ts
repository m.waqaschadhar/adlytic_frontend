import { Component, OnInit,ViewChild } from '@angular/core';
import { Chart } from 'chart.js';
import { viewParentEl } from '@angular/core/src/view/util';
import { RequestService } from 'src/app/request.service';
import { MatDialogRef, MatDialog,MatSnackBar } from '@angular/material';
import { FormControl } from '@angular/forms';
import { arrayMax } from 'highcharts/highcharts.src';
import { forEach } from '@angular/router/src/utils/collection';
import { version } from 'punycode';
import {ActivatedRoute } from '@angular/router';

import 'chartjs-plugin-zoom';
import 'chartjs-plugin-labels';


@Component({
  selector: 'app-userdata',
  templateUrl: './userdata.component.html',
  styleUrls: ['./userdata.component.scss']
})
export class UserdataComponent implements OnInit {
  @ViewChild('lineChart') private chartRef;
  @ViewChild('piechart') private pieref;
  @ViewChild('piechartdrc') private dirref; 
  @ViewChild('barchart') private barref; 
  
  number:any=1;
  chart:any;
  piechart: any;
  barchart:any;
  piechartdrc:any;

  tokenParam:any;
  locationID:any;
  locationName:any;

  AccessStatus:any;

  today:Date = new Date();
  colours:any = { car:"#264b96",bus:"#bf212f",truck:"#006f3c",bike:"#f9a73e"};
  vehcolor:any=["green","purple","yellow","blue"];

  defaultveh:any = ["car","bus","truck","bike"];
  
  vehicles:any =  [
    { text: "Bike", value: "bike" },
    { text: "Car", value: "car" },
    { text: "Bus", value: "bus" },
    { text: "Truck", value: "truck" }
  ];

  directions:any ;

   
  requestData:any = {
    locationId: '',
    start: new Date(this.today.getFullYear(), this.today.getMonth(), this.today.getDate(), 0, 0, 0),
    end: new Date(this.today.getFullYear(), this.today.getMonth(), this.today.getDate(), 23, 59, 59),
    vehicle: new FormControl(),
    direction: new FormControl()
  }
  

  constructor( private snackBar:MatSnackBar,public dialog: MatDialog, private requestService:RequestService , private  route: ActivatedRoute,) { }


  ngOnInit() {
  this.route.paramMap.subscribe(params => {
    this.tokenParam = params.get("id");
    const res = this.getlocationInfo(this.tokenParam);
   
  });
      // this.requestService.getlocationID(this.tokenParam).subscribe(res=>{
      //    this.locationID = (status)? res.locationid: null;
      // }, err => this.locationID=null);
}


getlocationInfo(token){
  let a = 1;
  
this.requestService.getlocationid(token).subscribe( res=> this.locationIdSuccess(res),err=>this.locationerr(err));
  
}
async locationIdSuccess(res){
  this.requestData.locationId= res.locationName;
  //this.locationName= res.locationname;
  this.locationID=res.locationid;
  await this.requestService.getdirectionbyname(this.requestData.locationId).subscribe(res=>this.directionssuccess(res),err=>this.directionerror(err));
  if(this.number==1){
    
  this.requestService.getAnalyticsbyid(res.locationName).subscribe(res=>this.AnalyticSuccessByID(res,this.defaultveh),err=>this.timelineErrorResponse(err));
  }
  this.AccessStatus= res.status;
  if(this.requestData.locationId==null){
    this.locationerr("zero");
  }
}
async directionssuccess(res){
this.directions=res;

}
directionerror(err){
  console.log(err);
}
locationerr(err:any){
  this.snackBar.open("It Seems you dont have permission to access ,please contact to service Provider", "OK", {
  duration: 10000,
  });
    if(this.chart){
      this.chart.destroy();
      this.piechart.destroy();
      this.barchart.destroy();
      this.piechartdrc.destroy();
    }

}

AnalyticSuccessByID(res,req){

  console.log("direccall",this.directions);
 
  
  this.number += 1;
  
  const vardata = res;
   let data1=[];
   let counter ={};
   let childdata=[];
   let totalcount=[];
   let pcufactor=[];
   let totalveh =[];
   let starttime=[];
     vardata.forEach(element => {
             data1.push(element.category)
             var start = new Date(element.start);
           starttime.push( start.getHours()+":"+start.getMinutes());
           totalcount.push(element.total);
           pcufactor.push(element.pcu);
           var key =  element.direction;
             counter[key] = (counter[key] || 0)+1;
   })
      
      const arr= req;
      
       const directiondetail= this.getdirectiondetail( Object.keys(counter),res);
     
         for(var i=0;i<arr.length;i++){ 
           const result= this.getarray(arr[i],data1);
           const sum= this.getSumofVeh(arr[i],data1)
             childdata.push(result);
             totalveh.push(sum);
         }
        if(this.chart){
         this.chart.destroy();
         this.piechart.destroy();
         this.barchart.destroy();
         this.piechartdrc.destroy();
        }
         this.chart = this.generateChart("Timeline", childdata,starttime);
         this.piechart = this.generatepieChart("Vehicle Detail", totalveh,arr);
         this.barchart = this.generatebarChart("Vehicle Total", totalcount , pcufactor,starttime);
         this.piechart = this.generatepiedirection("Vehicle Detail", directiondetail,arr);

         
 }

 async getAnalytics(){
  
    await  this.getlocationInfo(this.tokenParam);
      console.log("AccessStatus",this.AccessStatus);
      if(this.AccessStatus===true){
    let startDate = this.requestData.start;
    
    startDate = new Date(startDate.getFullYear(), startDate.getMonth(), startDate.getDate(), 0, 0, 0);
    let endDate = this.requestData.end;
    endDate = new Date(endDate.getFullYear(), endDate.getMonth(), endDate.getDate(), 23, 59, 59);
    let vehicles = this.requestData.vehicle.value.join(',');
    let splitarr = vehicles.split(",");
    
    let directions = this.requestData.direction.value.join(',');
    let direcsplit = directions.split(",");
    
    let requestData = {locationId: this.requestData.locationId, start: startDate.getTime(), end: endDate.getTime(), vehicle: vehicles, direction: directions};
    console.log('formData:',requestData);
    this.requestService.getAnalytics(requestData).subscribe(res=>this.timelineSuccessResponse(res,splitarr,direcsplit), err=>this.timelineErrorResponse(err));
      }else{
        this.locationerr("Access Denied");
      }
  }

  timelineSuccessResponse(res,req,direcsplit){
  console.log("requestresponse",res);
     const vardata = res;
      let data1=[];
      let counter={};
      let childdata=[];
      let totalcount=[];
      let pcufactor=[];
      let totalveh =[];
      let starttime=[];
        vardata.forEach(element => {
                data1.push(element.category)
                var start = new Date(element.start);
              starttime.push( start.getHours()+":"+start.getMinutes());
              totalcount.push(element.total);
              pcufactor.push(element.pcu);
                  
      })
      const directiondetail= this.getdirectiondetail(direcsplit,res);
      console.log("Counteris",directiondetail);
         // let arr = Object.keys(data1[0]);
          const arr= req;
          //console.log("array from request",req.split(','));
            for(var i=0;i<arr.length;i++){ 
              const result= this.getarray(arr[i],data1);
              const sum= this.getSumofVeh(arr[i],data1)
                childdata.push(result);
                totalveh.push(sum);
            }
           if(this.chart){
            this.chart.destroy();
            this.piechart.destroy();
            this.barchart.destroy();
           // this.piechartdrc.destroy();
           }
            this.chart = this.generateChart("Timeline", childdata,starttime);
            this.piechart = this.generatepieChart("Total", totalveh,arr);
            this.barchart = this.generatebarChart("Vehicle Total", totalcount , pcufactor,starttime);   
            this.piechartdrc = this.generatepiedirection("Direction Detail", directiondetail,arr);
          }

          getdirectiondetail(dir,res){
            const result={};
           
            dir.forEach(element => {
              let sum =0;  
              for(let data of res){
                  if (data.direction == element){
                    sum +=  parseInt( data.total);
                  }
                }
                result[element]=sum;

            });
            return result;
          }
   
    generatebarChart(title,total,pcu,label){
      return new Chart(this.barref.nativeElement, {
        type: 'line', 
        data: {
          labels: label, // your labels array
          datasets: [
            {
              data: total, // your data array
              backgroundColor: '#264b96',
              borderColor:'#264b96',
              label:title,
              type:'line',
              fill: false,
              
            },
            {
              data: pcu, // your data array
              backgroundColor: '#f9a73e',
              borderColor:'#f9a73e',
              label:"PCU Factor",
              fill: false,
              type:'line'
              
            }
           
          ]
        },
        options: {
          legend: {
            display: true
          },
          title: {
            display: true,
            text: 'Analytics'
        },
          scales: {
            xAxes: [{
              display: true,
              ticks: {
              
                beginAtZero: true
            }
            }
           
          ],
            yAxes: [{
              display: true,
              ticks: {
                
                beginAtZero: true
            }
            }],
          },
          responsive:true,
          maintainAspectRatio: false,
           
        }
      });

    }

    generatepieChart(title,data,label){
     let colors=[];
      label.forEach(element => {
        colors.push(this.colours[element]);
      });
     console.log('colorlabels',label);
      return new Chart(this.pieref.nativeElement,{
        
        type: 'doughnut',
        data: {
          labels: label,
          datasets: [{
            label: "Population (millions)",
            backgroundColor: colors,
            data: data
          }]
        },
        options: {
          plugins:{
            labels: {
              render: 'percentage',
              fontColor: "white",
              precision: 2
            }
          },
          title: {
            display: true,
            text: title
          },
         cutoutPercentage:50,
         responsive:true,
         maintainAspectRatio: false,
          
        }
      });

    }

    generatepiedirection(title,dataset,label){
      let colors=[];
       label.forEach(element => {
         colors.push(this.colours[element]);
       });
       let values = Object.values(dataset);
       return new Chart(this.dirref.nativeElement,{
         
         type: 'doughnut',
         data: {
           labels: Object.keys(dataset),
           datasets: [{
             
             backgroundColor: colors,
             data: values
           }]
         },
         options: {
           plugins:{
             labels: {
               render: 'percentage',
               fontColor: "white",
               precision: 2
             }
           },
           title: {
             display: true,
             text: title
           },
          cutoutPercentage:50,
          responsive:true,
          maintainAspectRatio: false,
           
         }
       });
 
     }
 

    generateChart(title,dataset,time){
      return new Chart(this.chartRef.nativeElement, {
    
        type: 'line',
      
        data: {
          labels: time, // your labels array
          datasets: dataset
        },
        options: {
            legend: {
            display: true
          },
          title: {
            display: true,
            text: title
            },
          scales: {
            xAxes: [{
              display: true,
              ticks: { 
                beginAtZero: true
            }
            }],
            yAxes: [{
              display: true,
              ticks: {
                beginAtZero: true
            }
            }],
          },
          plugins: {
            zoom: {
              pan: {
                enabled: false,
                mode: 'x'
              },
              zoom: {
                enabled: false,
                 mode: 'x',
              }
            }
          },
          responsive:true,
          maintainAspectRatio: false,
        }
      });
    } 

      getarray(name,arr){
          let valu=[];
        arr.forEach(element => {
          valu.push(element[name]);
        });
        const res = {          
          data:valu,
          borderColor: this.colours[name],
          borderWidth:2,
          label:name,
          fill:'false'
        }
        return res;
      }

      getSumofVeh(name,arr){
        let sum=0;
        arr.forEach(element => {
       
          sum = sum + parseInt(element[name]);

        });
          return sum;

      }
      
          
      
    
  
  timelineErrorResponse(err){
    console.log(err);
  }
 
  refreshbar(){
console.log("bar clicked");
    this.barchart.update();
  }
  refreshpie(){
   // console.log("pie clicked",chart);
    this.piechart.update();
  }
  refreshline(){
    console.log("line clicked");
    this.chart.update();
  }
 

}
