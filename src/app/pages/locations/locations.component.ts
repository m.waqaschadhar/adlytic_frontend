import { Component, OnInit } from '@angular/core';
import { AppDataService } from 'src/app/app-data.service';
import { FormControl } from '@angular/forms';
import { RequestService } from 'src/app/request.service';

@Component({
  selector: 'app-locations',
  templateUrl: './locations.component.html',
  styleUrls: ['./locations.component.scss']
})
export class LocationsComponent implements OnInit {

  countries;
  requestData:any = {
    city: new FormControl(),
    country: new FormControl()
  };
  lat: number = 51.678418;
  lng: number = 7.809007;
  locations:any = [];
  showMaps = false;
  constructor(private appData:AppDataService, private requestService:RequestService) { 
    this.countries = appData.countries;
    let user = this.appData.getUser();
    this.requestData = {
      city: new FormControl(user.city? user.city : "Lahore"),
      country: new FormControl(user.country? user.country : "Pakistan")
    }
  }



  ngOnInit() {
    this.getLocations();
  }

  getLocations(){
    let data = {city: this.requestData.city.value, country: this.requestData.country.value};
    this.requestService.getLocations(data).subscribe(res=>this.locationsSuccess(res));
  }

  locationsSuccess(response){
    console.log(response);
    this.locations = response;
    if(response.length){
      this.lat = parseFloat(response[0].latitude);
      this.lng = parseFloat(response[0].longitude);
      this.showMaps = true;
    }
  }

}
