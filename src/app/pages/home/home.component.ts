import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialogRef, MatDialog } from '@angular/material';
import { forkJoin } from 'rxjs';
import { RequestService } from 'src/app/request.service';
import { Chart, HIGHCHARTS_MODULES } from 'angular-highcharts';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  @ViewChild('file') file;
  public files: Set<File> = new Set();

  progress;
  analytics:any;
  locationId:number;
  directionChart:Chart;
  vehicleChart:Chart;
  timelineChart:Chart;
  today:Date = new Date();

  vehicles:any =  [
    { text: "Bike", value: "bike" },
    { text: "Car", value: "car" },
    { text: "Bus", value: "bus" },
    { text: "Truck", value: "truck" }
  ];

  directions:any =  [
    { text: "Front", value: "front" },
    { text: "Left", value: "left" },
    { text: "Right", value: "right" }
  ];

  requestData:any = {
    locationId: '',
    start: new Date(this.today.getFullYear(), this.today.getMonth(), this.today.getDate(), 0, 0, 0),
    end: new Date(this.today.getFullYear(), this.today.getMonth(), this.today.getDate(), 23, 59, 59),
    vehicle: new FormControl(),
    direction: new FormControl()
  }
  constructor( public dialog: MatDialog, private requestService:RequestService ) { }

  ngOnInit() {

  }

  getAnalytics(){
    let startDate = this.requestData.start;
    
    startDate = new Date(startDate.getFullYear(), startDate.getMonth(), startDate.getDate(), 0, 0, 0);
    let endDate = this.requestData.end;
    endDate = new Date(endDate.getFullYear(), endDate.getMonth(), endDate.getDate(), 23, 59, 59);
    let vehicles = this.requestData.vehicle.value.join(',');
    let directions = this.requestData.direction.value.join(',');
    let requestData = {locationId: this.requestData.locationId, start: startDate.getTime(), end: endDate.getTime(), vehicle: vehicles, direction: directions};
    console.log('formData:',requestData);
    this.requestService.getAnalytics(requestData).subscribe(res=>this.successResponse(res), err=>this.errorResponse(err));
    this.getTimeline(requestData);
  }

  successResponse(response){
    console.log("Success Response",response);
    let directionData, vehicleData;
    if(response.length){
      this.analytics = response[0];
      response = response[0];
      directionData = [
        {y: response.left, name: "Left ("+response.left+")", color:'#28a745', legendIndex: 1},
        {y: response.right, name: "Right ("+response.right+")", color:'#007bff', legendIndex: 2},
        {y: response.front, name: "Front ("+response.front+")", color:'#ffc107', legendIndex: 3}
      ];

      vehicleData = [
        {y: response.bike, name: "Bike ("+response.bike+")", color:'#28a745'},
        {y: response.car, name: "Car ("+response.car+")", color:'#007bff'},
        {y: response.bus, name: "Bus ("+response.bus+")", color:'#ffc107'},
        {y: response.truck, name: "Truck ("+response.truck+")", color:'#dd7bff'}
      ];
    }else{
      directionData = [
        {y: 0, name: "Left (0)", color:'#28a745'},
        {y: 0, name: "Right (0)", color:'#007bff'},
        {y: 0, name: "Front (0)", color:'#ffc107'}
      ];

      vehicleData = [
        {y: 0, name: "Bike (0)", color:'#28a745'},
        {y: 0, name: "Car (0)", color:'#007bff'},
        {y: 0, name: "Bus (0)", color:'#ffc107'},
        {y: 0, name: "Truck (0)", color:'#dd7bff'}
      ];
    }

    this.directionChart = this.generateChart("Direction Statistics", directionData);
    this.vehicleChart = this.generateChart("Vehicle Statistics", vehicleData);
  }

  errorResponse(error){
    console.log(error);
  }

  generateChart(title, data){
    let seriesOptions:any = {
      type: "pie",
      name:"",
      data: data,
      innerSize: '80%',
      minSize: '100px',
      size: '200px',
      dataLabels: {
        enabled: true,
        overflow: false,
        crop: false
      },
      startAngle: 90
    };
    return new Chart({
      title: {
        text: title
      },
      credits: {
        enabled: false
      },
      series: [seriesOptions],
      colors: ['#28a745', '#007bff', '#ffc107']
    });
  }



  getTimeline(requestData){
    this.requestService.getTimeline(requestData).subscribe(res=>this.timelineSuccessResponse(res), err=>this.timelineErrorResponse(err));
  }


  toTime(hour:number){
    hour = hour+5;
    let rem = hour%12;
    let ampm = hour/12;
    if(ampm == 0){
      return rem+"AM";
    }else{
      return rem+"PM";
    }
  }

  toDate(day, month){
    let months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Nov", "Dec"];
    return months[month-1] + ", "+day;
  }

  timelineSuccessResponse(response){
    console.log(response);
    let timelineData;
    if(response.length){
      timelineData = [];
      for(let i=0; i<response.length; i++){
        let label = this.toTime(response[i]._id.hour)+' '+this.toDate(response[i]._id.day, response[i]._id.month);
        timelineData[i]= [label, response[i].total];
      }
    }else{
      timelineData = [];
    }
    console.log("Timeline Data:", timelineData);
    this.timelineChart = this.generateTimelineChart("Hourly Timeline", timelineData);
  }
  timelineErrorResponse(error){
    console.log(error);
  }

  generateTimelineChart(title, data){
    let seriesOptions:any = {
      chart: {
          type: 'column'
      },
      title: {
          text: title
      },
      xAxis: {
          type: 'category',
          labels: {
              rotation: -45,
              style: {
                  fontSize: '13px',
                  fontFamily: 'Verdana, sans-serif'
              }
          }
      },
      yAxis: {
          min: 0,
          title: {
              text: "Impressions"
          }
      },
      legend: {
          enabled: false
      },
      colors: ['#007bff'],
      series: [
        {
          name: 'Impressions',
          data: data,
          pointWidth: 20,
          dataLabels: {
            enabled: true,
            rotation: -90,
            color: '#FFFFFF',
            align: 'right',
            format: '{point.y}',
            y: 10,
            style: {
              fontSize: '13px',
              fontFamily: 'Verdana, sans-serif'
            }
          }
        }
      ]
    };
    return new Chart(seriesOptions);
  }

}