import { Component, OnInit,ViewChild } from '@angular/core';
import { RequestService } from 'src/app/request.service';
import { MatDialog ,MatSnackBar,MatTableDataSource, MatSort, MatPaginator} from '@angular/material';
import { SharelinkModelComponent } from 'src/app/modal/sharelink-model/sharelink-model.component';
import { VewlinkModalComponent } from 'src/app/modal/vewlink-modal/vewlink-modal.component';

declare var $;
@Component({
  selector: 'app-share',
  templateUrl: './share.component.html',
  styleUrls: ['./share.component.scss']
})
export class ShareComponent implements OnInit {

  @ViewChild('dataTable') table;
  dataTable: any;
  locationdata: any;
  locationid : any;
  tokendata:any;
  linkpanel : boolean =false;
  panelOpenState: boolean = false;
  baseurl = "http://localhost:4200/data/user/";
  url:any;
  displayedColumns: string[] = ['serial','location', 'direction', 'action'];
 
    @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(  private requestService:RequestService,public dialog: MatDialog,private snackBar: MatSnackBar) { }
  togglePanel(){
    this.panelOpenState = !this.panelOpenState
        }
  ngOnInit():void {
      this.requestService.getlocations().subscribe(res=> this.locationsSuccess(res),err=> this.locationerr(err));
    }

  locationsSuccess(res){
    this.locationdata = res;
    this.locationdata.paginator = this.paginator;
  }
  locationerr(err){

  }
  OpenDilog(id){
    console.log(id);
    this.locationid=id;
    //SharelinkModelComponent
    const dialogRef = this.dialog.open(SharelinkModelComponent, {
      height: 'auto',
      width: '40%',
      disableClose: false
    });
    dialogRef.afterClosed().subscribe(result => {
      if(result==undefined){

      }else if(result==null){

      }else{
        const payload= {
          title: result.title,
          id: this.locationid
        }
        this.requestService.saveloctoken(payload).subscribe(res=> this.tokensuccess(res),err=> this.tokenerror(err));  
      }
    });
  }

  OpeneditDilog(id){
    const dialogRef = this.dialog.open(VewlinkModalComponent, {
      height: 'auto',
      width: '40%',
      disableClose: false
    });
    dialogRef.afterClosed().subscribe(result => {
      if(result==undefined){}
      else if(result==null){}
      else{
        console.log(result);
        if(result.status==="delete"){
          this.requestService.deletetokenbyid(id).subscribe(res=> this.deletesuccess(res),err=> this.deleteerror(err));
        }
        else{
          const payload= {
            status: result.status,
            id: id
          }
          this.requestService.updatetokenbyid(payload).subscribe(res=> this.updatesuccess(res),err=> this.updateerror(err));
        }        
      }
    });
  }

  deletesuccess(res:any){
    this.snackBar.open("Token Deleted Successfully", "", {
      duration: 2000,
    });
   
  }
  deleteerror(err:any){
    console.log(err);
  }
  updatesuccess(res:any){

    this.snackBar.open("Status Updated Successfully", "", {
      duration: 2000,
    });
  }
  updateerror(err:any){
    console.log(err);
  }

  tokensuccess(res){
      this.snackBar.open("Token Generated Successfully", "", {
        duration: 2000,
      });
  }

  tokenerror(err){
    console.log(err);
  }

  Openlinks(id){ 
    this.requestService.gettokensbyID(id).subscribe(res=> this.loctokensuccess(res),err=>this.loctokenerror(err));
  }

  loctokensuccess(res){
    this.tokendata= res;
    this.linkpanel=true;
  }
  loctokenerror(err){
    console.log(err)
  }
  copytoclipboard(text){
    let selBox = document.createElement('textarea');
    selBox.style.position = 'fixed';
    selBox.style.left = '0';
    selBox.style.top = '0';
    selBox.style.opacity = '0';
    selBox.value = text;
    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand('copy');
    document.body.removeChild(selBox);
    this.snackBar.open("Url Copied to clipboard","",{
      duration: 2000,
    });

  }


}
