import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { RequestService } from 'src/app/request.service';
import { CampaignModalComponent } from 'src/app/modal/campaign-modal/campaign-modal.component';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-campaign',
  templateUrl: './campaign.component.html',
  styleUrls: ['./campaign.component.scss']
})
export class CampaignComponent implements OnInit {

  baseUrl:string;
  brands:any[];
  campaigns:any[];
  constructor(public dialog: MatDialog, private requestService:RequestService, private sanitizer:DomSanitizer) {}

  ngOnInit() {
    this.baseUrl = this.requestService.getBaseUrl() + 'uploads/';
    this.getBrands();
  }


  openDialog() {
    const dialogRef = this.dialog.open(CampaignModalComponent, {
      height: 'auto',
      width: '400px',
      disableClose: true,
      data: this.brands
    });
    dialogRef.afterClosed().subscribe(result => {
      if(result==undefined){

      }else if(result==null){

      }else{
        this.addCampaign(result);
      }
    });
  }

  getProgress(campaign){
    let startDate = new Date(campaign.start_date);
    let endDate = new Date(campaign.end_date);
    let today = new Date();
    if(today.getTime()<startDate.getTime()){
      return 0;
    }
    let progress = 100 * (today.getTime() - startDate.getTime())/(endDate.getTime() - startDate.getTime());
    return progress;
  }

  getImpressionProgress(campaign){
    let impressions = 7000;
    let progress = 100 * impressions/campaign.impressions;
    return progress;
  }

  getSecure(url:string){
    return this.sanitizer.bypassSecurityTrustStyle(`url(${url})`);
  }

  addCampaign(requestData){
    this.requestService.createCampaign(requestData).subscribe(res=>this.addSuccess(res), err=>this.addError(err));
  }

  addSuccess(response:any){
    console.log(response);
  }

  addError(error:any){
    console.error(error);
  }

  getCampaigns(){
    this.requestService.getCampaigns().subscribe(res=>this.getSuccess(res), err=>this.getError(err));
  }

  getSuccess(response:any){
    console.log(response);
    this.campaigns = response;
    for(let i=0; i<this.campaigns.length; i++){
      this.campaigns[i].brand = this.brands.filter(brand=> brand._id == this.campaigns[i].brand)[0];
    }

  }

  getError(error:any){
    console.error(error);
  }

  getBrands(){
    this.requestService.getBrands().subscribe(res=>this.brandSuccess(res), err=>this.brandError(err));
  }

  brandSuccess(response:any){
    console.log(response);
    this.brands = response;
    this.getCampaigns();
  }

  brandError(error:any){
    console.error(error);
  }

}
