import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { RequestService } from 'src/app/request.service';
import { AppDataService } from 'src/app/app-data.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  
  form:FormGroup;
  showLoader:boolean = false;
  showError:boolean = false;
  showSuccess:boolean = false;
  errorMessage:string = "";
  successMessage:string = "";
  constructor(private requestService:RequestService, private dataService:AppDataService) { }

  ngOnInit() {
    this.init();
  }

  init(){
    this.form = new FormGroup({
      username: new FormControl('', [Validators.required, Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$')]),
      password: new FormControl('', [Validators.required, Validators.minLength(6), Validators.maxLength(16)]),
    });
  }

  login() : void {
    this.showLoader = true;
    this.showError = false;
    this.showSuccess = false;
    let userData = this.form.value;
    this.requestService.login(userData).subscribe(res => this.loginSuccess(res), err => this.loginError(err));
  }

  loginSuccess(response:any){
    this.form.reset();
    this.showSuccess = true;
    this.showLoader = false;
    this.successMessage = "Success!";
    response.login = true;
    this.dataService.setUser(response);
    this.dataService.routeMeTo('home');
  }

  loginError(error:any){
    this.errorMessage = error.error.message;
    this.showError = true;
    this.showLoader = false;
  }

}
