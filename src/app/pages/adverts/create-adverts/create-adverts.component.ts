import { Component, OnInit } from '@angular/core';
import { RequestService } from 'src/app/request.service';
import { AppDataService } from 'src/app/app-data.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UploadModalComponent } from 'src/app/modal/upload-modal/upload-modal.component';
import { MatDialog } from '@angular/material';

@Component({
  selector: 'app-create-adverts',
  templateUrl: './create-adverts.component.html',
  styleUrls: ['./create-adverts.component.scss']
})
export class CreateAdvertsComponent implements OnInit {

  isLinear = true;
  step1:FormGroup;
  step2:FormGroup;
  step3:FormGroup;
  step4:FormGroup;
  path:string = "";
  media:string = "";
  kinds:any = ["Image", "Video"];
  orientations:any = ["Portrait", "Landscape"];
  constructor(public dialog: MatDialog, private requestService: RequestService, private appData:AppDataService) { }

  ngOnInit(){
    this.initStep1();
    this.initStep2();
    this.initStep3();
    this.initStep4();
  }

  initStep1(){
    this.step1 = new FormGroup({
      title: new FormControl('', [Validators.required]),
      kind: new FormControl('', [Validators.required]),
      orientation: new FormControl('', [Validators.required]),
      brand: new FormControl('', [Validators.required]),
      product: new FormControl('', [Validators.required]),
      category: new FormControl('', [Validators.required]),
    });
  }

  initStep2(){
    this.step2 = new FormGroup({
      media: new FormControl('', [Validators.required]),
    });
  }

  initStep3(){
    this.step3 = new FormGroup({
      gender: new FormControl('', [Validators.required]),
      age: new FormControl('', [Validators.required]),
      locations: new FormControl('', [Validators.required]),
    });
  }

  initStep4(){
    this.step4 = new FormGroup({
      budget: new FormControl('', [Validators.required]),
      impressions: new FormControl('', [Validators.required]),
      start_on: new FormControl('', [Validators.required]),
      stop_on: new FormControl('', [Validators.required]),
    });
  }

  uploadDialog(contentType:string){
    const dialogRef = this.dialog.open(UploadModalComponent, {
      height: 'auto',
      width: '600px',
      disableClose: true,
      data: {
        type: contentType
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log(result);
      if(result==undefined) return;
      if(result==null) return;
      this.path = this.requestService.getBaseUrl()+'uploads/';
      this.media = result;
    });
  }

}
