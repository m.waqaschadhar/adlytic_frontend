import { Component, OnInit } from '@angular/core';
import { MatDialog, MatSnackBar } from '@angular/material';
import { RequestService } from 'src/app/request.service';
import { AppDataService } from 'src/app/app-data.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.component.html',
  styleUrls: ['./edit-profile.component.scss']
})
export class EditProfileComponent implements OnInit {

  user:any = null;
  organization:any = null;
  form:FormGroup;
  form1:FormGroup;
  showForm:boolean = false;
  countries;
  currencies;
  savingCompany = false;
  savingProfile = false;
  constructor(private _snackBar: MatSnackBar, public dialog: MatDialog, private requestService:RequestService, private dataService:AppDataService) {
    this.countries = dataService.countries; 
    this.currencies = dataService.currencies; 
  }

  ngOnInit() {
    this.user = this.dataService.getUser();
    this.getUser(this.user._id);
  }

  getUser(id){
    this.requestService.getUser(id).subscribe(res=>this.userSuccess(res));
  }

  userSuccess(response){
    console.log(response);
    this.user = response;
    this.organization = response.organization ? response.organization[0] : null;
    this.formInit();
  }

  formInit(){
    this.form = new FormGroup({
      firstName: new FormControl(this.user.firstName),
      lastName: new FormControl(this.user.lastName),
      username: new FormControl(this.user.username),
      contact: new FormControl(this.user.contact),
      id: new FormControl(this.user.id),
      country: new FormControl(this.user.country),
      language: new FormControl(this.user.language),
      organization: new FormControl(this.user.organization),
      role: new FormControl(this.user.role),
      currency: new FormControl(this.user.currency)
    });
    this.form1 = new FormGroup({
      name: new FormControl(this.organization ? this.organization.name : ''),
      contact: new FormControl(this.organization ? this.organization.contact : ''),
      address: new FormControl(this.organization ? this.organization.address : ''),
      address2: new FormControl(this.organization ? this.organization.address2 : ''),
      city: new FormControl(this.organization ? this.organization.city : ''),
      country: new FormControl(this.organization ? this.organization.country : ''),
      postcode: new FormControl(this.organization ? this.organization.postcode : ''),
      currency: new FormControl(this.organization ? this.organization.currency : '')
    });
    this.showForm = true;
  }

  saveProfile(){
    this.savingProfile = true;
    let userData = this.form.value;
    this.requestService.saveProfile(userData).subscribe(res => this.successProfile(res))
  }

  successProfile(response){
    this.savingProfile = false;
    console.log(response);
    this.openSnackBar("Profile saved successfully.");
  }

  saveCompany(){
    this.savingCompany = true;
    let companyData = this.form1.value;
    this.user.organization = companyData;
    this.requestService.saveProfile(this.user).subscribe(res => this.successCompany(res))
  }

  successCompany(response){
    this.savingCompany = false;
    console.log(response);
    this.openSnackBar("Company information saved successfully.");
  }

  openSnackBar(message: string) {
    this._snackBar.open(message, "", {
      duration: 2000,
    });
  }

}
