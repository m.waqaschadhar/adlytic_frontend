import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { RequestService } from 'src/app/request.service';
import { AppDataService } from 'src/app/app-data.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  user:any = null;
  organization:any = null;
  constructor(public dialog: MatDialog, private requestService:RequestService, private dataService:AppDataService) { }

  ngOnInit() {
    this.user = this.dataService.getUser();
    this.getUser(this.user._id);
  }

  getUser(id){
    this.requestService.getUser(id).subscribe(res=>this.userSuccess(res));
  }

  userSuccess(response){
    console.log(response);
    this.user = response;
    this.organization = response.organization ? response.organization[0] : null;
  }

}
