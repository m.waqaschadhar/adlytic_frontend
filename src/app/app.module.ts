import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ChartModule, HIGHCHARTS_MODULES } from 'angular-highcharts';
import {DataTableModule} from "angular-6-datatable";
import * as more from 'highcharts/highcharts-more.src';
import * as exporting from 'highcharts/modules/exporting.src';
import {} from '@angular/material/expansion';
import {MatTableModule} from '@angular/material/table';

import {MatToolbarModule,MatTooltipModule,MatSortModule,MatExpansionModule, MatPaginatorModule, MatFormFieldModule, MatOptionModule, MatSelectModule, MatInputModule, MatIconModule, MatSidenavModule, MatListModule, MatButtonModule, MatMenuModule, MatBadgeModule, MatCardModule, MatProgressBarModule, MatDialogModule, MatGridListModule, MatRippleModule, MatProgressSpinnerModule, MatStepperModule, MatTabsModule, MatRadioModule, MatSnackBarModule, MatDatepickerModule, MatNativeDateModule, MatChipsModule} from '@angular/material';

import { AgmCoreModule } from '@agm/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LayoutComponent } from './layout/layout.component';
import { HeaderComponent } from './layout/header/header.component';
import { FooterComponent } from './layout/footer/footer.component';
import { HomeComponent } from './pages/home/home.component';
import { SidebarComponent } from './layout/sidebar/sidebar.component';
import { ProfileComponent } from './pages/profile/profile.component';
import { HttpClientModule } from '@angular/common/http';
import { LoginComponent } from './pages/login/login.component';
import { SignupComponent } from './pages/signup/signup.component';
import { ForgotComponent } from './pages/forgot/forgot.component';
import { ResetPasswordComponent } from './pages/reset-password/reset-password.component';
import { CampaignComponent } from './pages/campaign/campaign.component';
import { BrandComponent } from './pages/brand/brand.component';
import { BrandModalComponent } from './modal/brand-modal/brand-modal.component';
import { UploadModalComponent } from './modal/upload-modal/upload-modal.component';
import { MediaFilesComponent } from './pages/media-files/media-files.component';
import { MediaModalComponent } from './modal/media-modal/media-modal.component';
import { CampaignModalComponent } from './modal/campaign-modal/campaign-modal.component';
import { NotFoundComponent } from './pages/not-found/not-found.component';
import { EditProfileComponent } from './pages/profile/edit-profile/edit-profile.component';
import { EditCompanyComponent } from './pages/profile/edit-company/edit-company.component';
import { LocationsComponent } from './pages/locations/locations.component';
import { AdvertsComponent } from './pages/adverts/adverts.component';
import { CreateAdvertsComponent } from './pages/adverts/create-adverts/create-adverts.component';
import { UserdataComponent } from './pages/userdata/userdata.component';
import { UserlayoutComponent } from './userlayout/userlayout.component';
import { UserheaderComponent } from './userlayout/userheader/userheader.component';
import { UserfooterComponent } from './userlayout/userfooter/userfooter.component';
import { UsersidebarComponent } from './userlayout/usersidebar/usersidebar.component';
import { ShareComponent } from './pages/share/share.component';
import { SharelinkModelComponent } from './modal/sharelink-model/sharelink-model.component';
import { VewlinkModalComponent } from './modal/vewlink-modal/vewlink-modal.component';

@NgModule({
  declarations: [
    AppComponent,
    LayoutComponent,
    HeaderComponent,
    FooterComponent,
    HomeComponent,
    SidebarComponent,
    ProfileComponent,
    LoginComponent,
    SignupComponent,
    ForgotComponent,
    ResetPasswordComponent,
    CampaignComponent,
    BrandComponent,
    BrandModalComponent,
    UploadModalComponent,
    MediaFilesComponent,
    MediaModalComponent,
    CampaignModalComponent,
    NotFoundComponent,
    EditProfileComponent,
    EditCompanyComponent,
    LocationsComponent,
    AdvertsComponent,
    CreateAdvertsComponent,
    UserdataComponent,
    UserlayoutComponent,
    UserheaderComponent,
    UserfooterComponent,
    UsersidebarComponent,
    ShareComponent,
    SharelinkModelComponent,
    VewlinkModalComponent,
  ],
  imports: [
    BrowserAnimationsModule,
    BrowserModule,
    DataTableModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    HttpClientModule,
    RouterModule,
    AppRoutingModule,
    MatPaginatorModule,
    MatSortModule,
    MatExpansionModule,
    MatInputModule,
    MatToolbarModule,
    MatFormFieldModule,
    MatTableModule,
    MatOptionModule,
    MatSelectModule,
    MatIconModule,
    MatSidenavModule,
    MatListModule,
    MatButtonModule,
    MatMenuModule,
    MatBadgeModule,
    MatCardModule,
    MatProgressBarModule,
    MatDialogModule,
    MatGridListModule,
    MatRippleModule,
    MatProgressSpinnerModule,
    MatStepperModule,
    MatDialogModule,
    MatTabsModule,
    MatRadioModule,
    MatSnackBarModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatChipsModule,
    MatTooltipModule,
    ChartModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyA3-87FjuOHBnEMji5qwNwWQCc6nnmrCf0'
    })
  ],
  exports: [
    ReactiveFormsModule,
    BrandModalComponent,
    UploadModalComponent,
  ],
  entryComponents: [
    BrandModalComponent,
    CampaignModalComponent,
    UploadModalComponent,
    SharelinkModelComponent,
    VewlinkModalComponent

  ],
  providers: [{provide: HIGHCHARTS_MODULES, useFactory: () => [ more, exporting ]}],
  bootstrap: [AppComponent]
})
export class AppModule { }
