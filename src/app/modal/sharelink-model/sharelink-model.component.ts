import { Component, OnInit,Inject } from '@angular/core';
import { MatSnackBar, MatDialog,MAT_DIALOG_DATA } from '@angular/material';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-sharelink-model',
  templateUrl: './sharelink-model.component.html',
  styleUrls: ['./sharelink-model.component.scss']
})
export class SharelinkModelComponent implements OnInit {

  form:FormGroup;
  constructor(private snackBar: MatSnackBar, public dialog: MatDialog, @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit() {
    this.init();
  }

  init(){
    this.form = new FormGroup({
      title: new FormControl('', [Validators.required])
     
    });
  }

}
