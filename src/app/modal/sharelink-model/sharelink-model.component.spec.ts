import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SharelinkModelComponent } from './sharelink-model.component';

describe('SharelinkModelComponent', () => {
  let component: SharelinkModelComponent;
  let fixture: ComponentFixture<SharelinkModelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SharelinkModelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SharelinkModelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
