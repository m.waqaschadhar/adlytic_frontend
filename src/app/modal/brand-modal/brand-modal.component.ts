import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { RequestService } from 'src/app/request.service';
import { MatSnackBar, MatDialog } from '@angular/material';
import { UploadModalComponent } from '../upload-modal/upload-modal.component';
import { AppDataService } from 'src/app/app-data.service';

@Component({
  selector: 'app-brand-modal',
  templateUrl: './brand-modal.component.html',
  styleUrls: ['./brand-modal.component.scss']
})
export class BrandModalComponent implements OnInit {

  form:FormGroup;
  constructor(private appData:AppDataService, private requestService:RequestService, private snackBar: MatSnackBar, public dialog: MatDialog) { };
  path:string = '../../../assets/img/fa-image.png';
  logo:any = {url: ''};
  categories:string[] = [
    "Entertainment",
    "Food",
    "Clothing",
    "Electronics",
    "Fruits & Vegitables",
    "Vehicles",
    "Hardware",
    "Software",
    "Beverages"
  ];
  ngOnInit() {
    this.init();
  }

  init(){
    this.form = new FormGroup({
      title: new FormControl('', [Validators.required]),
      category: new FormControl('', [Validators.required]),
      logo: new FormControl(''),
      user: new FormControl(this.appData.getUser()._id),
    });
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2000,
    });
  }

  uploadDialog(contentType:string){
    const dialogRef = this.dialog.open(UploadModalComponent, {
      height: 'auto',
      width: '600px',
      disableClose: true,
      data: {
        type: contentType
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log(result);
      if(result==undefined) return;
      if(result==null) return;
      this.path = this.requestService.getBaseUrl()+'uploads/';
      this.logo = result;
    });
  }
}
