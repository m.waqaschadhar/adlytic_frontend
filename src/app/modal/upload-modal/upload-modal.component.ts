import { Component, OnInit, ViewChild, Inject, Output} from '@angular/core';
import {MAT_DIALOG_DATA, MatTabChangeEvent} from '@angular/material';
import { MatDialogRef, MatDialog } from '@angular/material';
import { forkJoin } from 'rxjs';
import { RequestService } from 'src/app/request.service';

@Component({
  selector: 'app-upload-modal',
  templateUrl: './upload-modal.component.html',
  styleUrls: ['./upload-modal.component.scss']
})
export class UploadModalComponent implements OnInit {

  @ViewChild('file') file;
  public files: Set<File> = new Set();

  progress;
  canBeClosed = true; 
  primaryButtonText = 'Upload';
  showCancelButton = true; 
  uploading = false;
  uploadSuccessful = false;
  media:any = [];
  tab:number = 0;
  selectedFile:any = null;
  baseUrl:string="";
  gettingFile = true;

  constructor(public dialog: MatDialog, private requestService:RequestService, @Inject(MAT_DIALOG_DATA) public data: any) {
  }

  ngOnInit() {
    this.baseUrl = this.requestService.getBaseUrl();
    console.log(this.baseUrl);
    this.init();
  }

  init(){
    this.files= new Set();
    this.progress = null;
    this.canBeClosed = true; 
    this.primaryButtonText = 'Upload';
    this.showCancelButton = true; 
    this.uploading = false;
    this.uploadSuccessful = false;
    this.getImages();
  }

  getImages(){
    this.requestService.getMedia().subscribe(res => this.success(res), err => this.error(err));
  }

  success(response){
    console.log(response);
    this.gettingFile = false;
    if(this.data.type=="video/*"){
      this.media = response.filter(file => file.contentType == 'video');
    }else{
      this.media = response.filter(file => file.contentType == 'image');
    }
    console.log(this.media);
  }

  error(response){
    console.log(response);
    this.gettingFile = false;
  }

  addFiles() {
    this.file.nativeElement.click();
  }

  onFilesAdded() {
    const files: { [key: string]: File } = this.file.nativeElement.files;
    for (let key in files) {
      if (!isNaN(parseInt(key))) {
        this.files.add(files[key]);
      }
    }
  }

  uploadFiles() {
    if (this.uploadSuccessful) {
      console.log("Upload successfull");
      this.uploading = false;
      this.uploadSuccessful = false;
      this.resetFiles();
      this.progress = null;
      return;
    }
    this.uploading = true;
    this.progress = this.requestService.upload(this.files);
    let allProgressObservables = [];
    for (let key in this.progress) {
      allProgressObservables.push(this.progress[key].progress);
    }
    this.canBeClosed = false;
    this.showCancelButton = false;
    forkJoin(allProgressObservables).subscribe(end => {
      this.init();
    });
  }

  resetFiles(){
    this.init();
  }

  tabChanged(tabChangeEvent: MatTabChangeEvent){
    this.tab = tabChangeEvent.index;
  }

  selectFile(event:any){
    console.log(event);
    this.selectedFile = event;
  }

}
