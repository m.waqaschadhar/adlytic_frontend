import { Component, OnInit ,Inject} from '@angular/core';
import { MatSnackBar, MatDialog ,MAT_DIALOG_DATA} from '@angular/material';
import { FormGroup, FormControl, Validators } from '@angular/forms';


@Component({
  selector: 'app-vewlink-modal',
  templateUrl: './vewlink-modal.component.html',
  styleUrls: ['./vewlink-modal.component.scss']
})
export class VewlinkModalComponent implements OnInit {

  form:FormGroup;
  constructor( private snackBar: MatSnackBar, public dialog: MatDialog, @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit() {
    this.form = new FormGroup({
      status: new FormControl('', [Validators.required]) 
    });
  }

}
