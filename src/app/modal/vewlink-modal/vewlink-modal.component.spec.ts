import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VewlinkModalComponent } from './vewlink-modal.component';

describe('VewlinkModalComponent', () => {
  let component: VewlinkModalComponent;
  let fixture: ComponentFixture<VewlinkModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VewlinkModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VewlinkModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
