import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { RequestService } from 'src/app/request.service';
import { MatSnackBar, MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { UploadModalComponent } from '../upload-modal/upload-modal.component';
import { AppDataService } from 'src/app/app-data.service';

@Component({
  selector: 'app-campaign-modal',
  templateUrl: './campaign-modal.component.html',
  styleUrls: ['./campaign-modal.component.scss']
})
export class CampaignModalComponent implements OnInit {

  form:FormGroup;
  baseUrl:string = "";
  path:string = '../../../assets/img/fa-image.png';
  video:any = {url: '', thumbnail: ''};
  brands:any[] = [];
  ages:string[] = [
    "Kids",
    "Youngs",
    "Adults",
    "Old",
  ];

  genders:string[] = [
    "Male",
    "Female"
  ];

  constructor(private appData:AppDataService, private requestService:RequestService, private snackBar: MatSnackBar, public dialog: MatDialog, @Inject(MAT_DIALOG_DATA) public data: any) { };

  ngOnInit() {
    this.init();
    this.baseUrl = this.requestService.getBaseUrl()+'uploads/';
    this.brands = this.data;
    console.log(this.brands);
  }

  init(){
    this.form = new FormGroup({
      title: new FormControl('', [Validators.required]),
      brand: new FormControl('', [Validators.required]),
      video: new FormControl('', [Validators.required]),
      category: new FormControl(''),
      gender: new FormControl('', [Validators.required]),
      age: new FormControl('', [Validators.required]),
      impressions: new FormControl('', [Validators.required]),
      start_date: new FormControl(new Date(), [Validators.required]),
      end_date: new FormControl(new Date(new Date().getTime()+86400000), [Validators.required]),
      user: new FormControl(this.appData.getUser()._id),
    });
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2000,
    });
  }

  uploadDialog(contentType:string){
    const dialogRef = this.dialog.open(UploadModalComponent, {
      height: 'auto',
      width: '600px',
      disableClose: true,
      data: {
        type: contentType
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log(result);
      if(result==undefined) return;
      if(result==null) return;
      this.path = this.requestService.getBaseUrl()+'uploads/';
      this.video = result;
    });
  }

}
