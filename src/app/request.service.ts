import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpEventType, HttpResponse, HttpHeaders } from '@angular/common/http';
import { Subject } from 'rxjs';
import { Observable } from 'rxjs';
import { AppDataService } from './app-data.service';

const url = 'http://localhost:3500/';
//const url = 'http://omno.ai:3500/';

@Injectable({
  providedIn: 'root'
})

export class RequestService {

  constructor(private http: HttpClient, private appData:AppDataService) {}

  public upload(files: Set<File>): { [key: string]: { progress: Observable<number> } } {

    const status: { [key: string]: { progress: Observable<number> } } = {};

    files.forEach(file => {
      const headers = new HttpHeaders({
        'Authorization': 'Bearer '+this.appData.getUser().token
      });
      const formData: FormData = new FormData();
      formData.append('file', file, file.name);
      const req = new HttpRequest('POST', url+'media', formData, {
        reportProgress: true,
        headers: headers
      });

      const progress = new Subject<number>();
      this.http.request(req).subscribe(event => {
        if (event.type === HttpEventType.UploadProgress) {
          const percentDone = Math.round(100 * event.loaded / event.total);
          progress.next(percentDone);
        } else if (event instanceof HttpResponse) {
          progress.complete();
        }
      });
      status[file.name] = {
        progress: progress.asObservable()
      };
    });
    return status;
  }

  getBaseUrl(){
    return url;
  }
e
  create(userData:any){
    return this.http.post(url+'users/register', userData);
  }

  login(authData:any){
    return this.http.post(url+'users/authenticate', authData);
  }

  forgot(userData:any){
    return this.http.post(url+'users/forgot', userData);
  }

  reset(authData:any, token){
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer '+token
    });
    return this.http.post(url+'users/reset', authData, { headers: headers });
  }

  saveProfile(userData:any){
    let token = this.appData.getUser().token;
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer '+token
    });
    return this.http.put(url+'users/'+userData.id, userData, { headers: headers });
  }

  getUser(id:string){
    let token = this.appData.getUser().token;
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer '+token
    });
    return this.http.get(url+'users/'+id, { headers: headers });
  }

  list(){
    let token = this.appData.getUser().token;
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer '+token
    });
    return this.http.get(url+'users/', { headers: headers });
  }

  getMedia(){
    let user = this.appData.getUser();
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer '+user.token
    });
    return this.http.post(url+'media/'+user._id, {}, { headers: headers });
  }

  getlocationid(reqtoken){
    let user = this.appData.getUser();
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer '+user.token
    });
    return this.http.get(url+'adlytic/getid/'+reqtoken, { headers: headers });
  }

  saveloctoken(reqdata){
    let user = this.appData.getUser();
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer '+user.token
    });
    return this.http.post(url+'adlytic/savetoken', { reqdata }, { headers: headers });
  }

  getlocations(){
    let user = this.appData.getUser();
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer '+user.token
    });
    return this.http.get(url+'adlytic/getMeta', { headers: headers });
  }
  getdirectionbyname(name){
    let user = this.appData.getUser();
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer '+user.token
    });
    return this.http.get(url+'adlytic/getMeta/'+name, { headers: headers });
  }
  gettokensbyID(locid){

    let user = this.appData.getUser();
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer '+user.token
    });
    return this.http.get(url+'adlytic/gettoken/'+locid,{ headers: headers });

  }

  createMedia(){
    let token = this.appData.getUser().token;
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer '+token
    });
    return this.http.post(url+'media',{url:'myimage.png', views:0},{ headers: headers });
  }

  /*

  Brand Requests

  */

  getBrands(){
    let token = this.appData.getUser().token;
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer '+token
    });
    return this.http.get(url+'brand',{ headers: headers });
  }

  createBrand(requestData:any){
    let token = this.appData.getUser().token;
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer '+token
    });
    return this.http.post(url+'brand',requestData,{ headers: headers });
  }

  /*

  Campaign Requests

  */

  getCampaigns(){
    let token = this.appData.getUser().token;
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer '+token
    });
    return this.http.get(url+'campaign',{ headers: headers });
  }

  createCampaign(requestData:any){
    let token = this.appData.getUser().token;
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer '+token
    });
    return this.http.post(url+'campaign',requestData,{ headers: headers });
  }

  /*

  Analytics Requests

  */

  getDashboard(locationId:any){
    let token = this.appData.getUser().token;
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer '+token
    });
    return this.http.get(url+'adlytic/dashboard/'+locationId, { headers: headers });
  }
  getAnalyticsbyid(id){
    return this.http.get(url+'adlytic/analytic/'+id);

  }
  getAnalytics(requestData:any){
    let token = this.appData.getUser().token;
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer '+token
    });
    return this.http.get(url+'adlytic/analytics/'+requestData.locationId+'/start/'+requestData.start+'/end/'+requestData.end+'/vehicle/'+requestData.vehicle+'/direction/'+requestData.direction, { headers: headers });
  }
  getlocationID(requestToken:any){

    return true
  }
  getTimeline(requestData:any){
    let token = this.appData.getUser().token;
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer '+token
    });
    return this.http.get(url+'adlytic/timeline/'+requestData.locationId+'/start/'+requestData.start+'/end/'+requestData.end+'/vehicle/'+requestData.vehicle+'/direction/'+requestData.direction, { headers: headers });
  }


  /*
    Locations
  */

  getLocations(requestData){
    let token = this.appData.getUser().token;
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer '+token
    });
    return this.http.get(url+'locations/city/'+requestData.city+'/country/'+requestData.country, { headers: headers });
  }

  deletetokenbyid(id){
    let token = this.appData.getUser().token;
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer '+token
    });
    return this.http.post(url+'adlytic/deletetokenbyid',{id},{headers:headers})
  }
  updatetokenbyid(request){
    let token = this.appData.getUser().token;
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer '+token
    });
    return this.http.post(url+'adlytic/updatetokenbyid',{request},{headers:headers})
  }
}
