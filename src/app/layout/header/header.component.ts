import { Component, OnInit } from '@angular/core';
import { AppDataService } from 'src/app/app-data.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  APP_TITLE:string;
  layout:any;
  constructor(private appData:AppDataService) {
    this.APP_TITLE = this.appData.getAppTitle();
  }

  ngOnInit() {
    this.appLayout();
  }

  appLayout(){
   
    this.appData.getAppLayout().subscribe(res => this.appLayoutSuccess(res));
  }

  appLayoutSuccess(response:any){
    
    this.layout = response;
  }

  toggleSidebar(){
    
    this.layout.sidebar = !this.layout.sidebar;
    this.appData.setAppLayout(this.layout);
  }

  logout(){
    this.appData.logout();
  }

  navigateTo(currentPage:string){
    this.layout.currentPage = currentPage;
    this.appData.setAppLayout(this.layout);
  }

}
