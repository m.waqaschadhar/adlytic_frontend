import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import { ProfileComponent } from './pages/profile/profile.component';
import { LoginComponent } from './pages/login/login.component';
import { SignupComponent } from './pages/signup/signup.component';
import { ForgotComponent } from './pages/forgot/forgot.component';
import { AuthGuard } from './auth.guard';
import { ResetPasswordComponent } from './pages/reset-password/reset-password.component';
import { BrandComponent } from './pages/brand/brand.component';
import { MediaFilesComponent } from './pages/media-files/media-files.component';
import { NotFoundComponent } from './pages/not-found/not-found.component';
import { EditProfileComponent } from './pages/profile/edit-profile/edit-profile.component';
import { LocationsComponent } from './pages/locations/locations.component';
import { AdvertsComponent } from './pages/adverts/adverts.component';
import { CreateAdvertsComponent } from './pages/adverts/create-adverts/create-adverts.component';
import{ UserdataComponent } from './pages/userdata/userdata.component';
import { ShareComponent } from './pages/share/share.component';


const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'signup', component: SignupComponent},
  { path: 'forgot', component: ForgotComponent},
  { path: 'reset_password', component: ResetPasswordComponent},
  { path: 'data/user/:id', component: UserdataComponent},
  { path: 'home', redirectTo: '', pathMatch: 'full', canActivate : [AuthGuard]},
  { path: '', component: HomeComponent, canActivate : [AuthGuard]},
  { path: 'profile', component: ProfileComponent, canActivate : [AuthGuard] },
  { path: 'profile/edit', component: EditProfileComponent, canActivate : [AuthGuard] },
  { path: 'locations', component: LocationsComponent, canActivate : [AuthGuard] },
  { path: 'adverts', component: AdvertsComponent, canActivate : [AuthGuard] },
  { path: 'adverts/create', component: CreateAdvertsComponent, canActivate : [AuthGuard] },
  { path: 'brands', component: BrandComponent, canActivate : [AuthGuard] },
  { path: 'media', component: MediaFilesComponent, canActivate : [AuthGuard] },
  { path: 'share', component: ShareComponent, canActivate : [AuthGuard] },
  { path: '404', component: NotFoundComponent, canActivate : [AuthGuard]},
  { path: '**', redirectTo: '404', canActivate : [AuthGuard]},
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes) 
  ],
  exports:[
    RouterModule
  ],
  declarations: []
})
export class AppRoutingModule { }
