import { Component, OnInit } from '@angular/core';
import { AppDataService } from 'src/app/app-data.service';

@Component({
  selector: 'app-usersidebar',
  templateUrl: './usersidebar.component.html',
  styleUrls: ['./usersidebar.component.scss']
})
export class UsersidebarComponent implements OnInit {

  layout:any;
  links:any = [
    {path:'/home', icon:'home', text:'Home', class:'side-link'},
  ];
  constructor(private appData:AppDataService) { }

  ngOnInit() {
    this.appLayout();
  }

  appLayout(){
    this.appData.getAppLayout().subscribe(res => this.appLayoutSuccess(res));
  }

  appLayoutSuccess(response:any){
    this.layout = response;
    console.log("Sidebar Layout", this.layout);
  }

  expandSidebar(){
    this.layout.expand = !this.layout.expand;
    this.appData.setAppLayout(this.layout);
  }

  navigateTo(currentPage:string){
    this.layout.currentPage = currentPage;
    this.appData.setAppLayout(this.layout);
  }

}
