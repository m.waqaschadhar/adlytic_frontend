import { Component, OnInit } from '@angular/core';
import { AppDataService } from 'src/app/app-data.service';

@Component({
  selector: 'app-userheader',
  templateUrl: './userheader.component.html',
  styleUrls: ['./userheader.component.scss']
})
export class UserheaderComponent implements OnInit {

  APP_TITLE:string;
  layout:any;
  constructor(private appData:AppDataService) {
    this.APP_TITLE = this.appData.getAppTitle();
  }

  ngOnInit() {
    this.appLayout();
  }

  appLayout(){
    this.appData.getAppLayout().subscribe(res => this.appLayoutSuccess(res));
  }

  appLayoutSuccess(response:any){
    this.layout = response;
    console.log("layoutRes",this.layout);
  }

  toggleSidebar(){
    this.layout.sidebar = !this.layout.sidebar;
    this.appData.setAppLayout(this.layout);
  }

  logout(){
    this.appData.logout();
  }

  
}
