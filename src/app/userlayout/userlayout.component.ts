import { Component, OnInit } from '@angular/core';
import { AppDataService } from '../app-data.service';
@Component({
  selector: 'app-userlayout',
  templateUrl: './userlayout.component.html',
  styleUrls: ['./userlayout.component.scss']
})
export class UserlayoutComponent implements OnInit {

  showFiller = false;
  layout:any;
  constructor(private appData:AppDataService) { 
    
  }
  ngOnInit() {
    this.appLayout();
  }

  appLayout(){
    this.appData.getAppLayout().subscribe(res => this.appLayoutSuccess(res));
  }

  appLayoutSuccess(response:any){
    this.layout = response;
  }

  close(str:string){
    this.layout.sidebar = !this.layout.sidebar;
    this.appData.setAppLayout(this.layout);
  }

}
